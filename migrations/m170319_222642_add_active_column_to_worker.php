<?php

use yii\db\Migration;

class m170319_222642_add_active_column_to_worker extends Migration
{
    public function up()
    {
        $this->addColumn('worker', 'active', $this->integer(1)->defaultValue(1));
    }

    public function down()
    {
        $this->dropColumn('worker', 'active');
    }

}
