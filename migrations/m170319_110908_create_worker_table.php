<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker`.
 */
class m170319_110908_create_worker_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('worker', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('worker');
    }
}
