<?php

use yii\db\Migration;

class m170321_183306_insert_base_entity extends Migration
{
    public function up()
    {
        $this->insert('eav_entity', [
            'entityName' => 'Worker',
            'entityModel' => 'app\models\Worker',
            'categoryId' => '1',
        ]);
    }

    public function down()
    {
        echo "m170321_183306_insert_base_entity cannot be reverted.\n";
        return false;
    }
}
