<?php

use yii\db\Migration;

/**
 * Handles the creation of table `worker_history`.
 */
class m170322_222852_create_worker_history_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('worker_history', [
            'id' => $this->primaryKey(),
            'worker_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'active' => $this->integer(1),
            'history' => $this->text(),
            'date' => $this->dateTime(),

        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('worker_history');
    }
}
