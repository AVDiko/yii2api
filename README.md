        **Install**

    1. Update Composer
    2. Create 'yii2api' database
    3. Use php ./yii migrate/up -p=@mirocow/eav/migrations
    4. Use php yii migrate


        **Exploitation**

    1. /api/worker controller -- manage workers

    - /api/worker - GET - view all worker
    - /api/worker/1 - GET - view worker with id = 1
    - /api/worker - POST - create worker (name required)
    - /api/worker/1 - PUT - update worker with id = 1
    - /api/worker/1 - DELETE - delete (deactivate) worker with id = 1

    2. /api/worker-history - view workers history

    - /api/worker-history - GET - view all changes
    - /api/worker-history/1 - GET - view record with id = 1
    - /api/worker-history?worker_id=1 - GET - view a history of working with id = 1
        attributed : worker_id (int), name (string), active (1 - active, 0 - delete),
                    from (unix time), to (unix time)
    - /api/worker-history?name=serg&from=1488319200&to1490994000 - view a
     history about all workers who named 'serg' by march 2017
