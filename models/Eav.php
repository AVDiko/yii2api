<?php
namespace app\models;

use mirocow\eav\models\EavAttribute;
use mirocow\eav\models\EavAttributeValue;
use mirocow\eav\models\EavEntity;
use mirocow\eav\models\EavEntitySearch;
use yii\base\Exception;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

class Eav extends ActiveRecord
{
    public $eavAttributes = [];

    /**
     * @param $name
     * @param $label
     * @param bool $required
     * @param null $defaultValue
     * @return EavAttribute|\yii\db\ActiveQuery
     * @throws Exception
     */
    public function createEavAttribute($name, $label, $required = false, $defaultValue = null)
    {
        $attr = $this->getEavAttribute($name);

        if (!$attr instanceof EavAttribute) {

            /** @var ActiveDataProvider $entity */
            $entity = (new EavEntitySearch())->search(['entityModel' => User::className()])->getModels();
            if (!isset($entity[0]) || !$entity[0] instanceof EavEntity) {
                throw new Exception('Eav entity not found');
            }

            $attr = new EavAttribute();
            $attr->attributes = [
                'entityId' => $entity[0]->id, // Category ID
                'name' => $name,  // service name field
                'label' => $label,         // label text for form
                'defaultValue' => $defaultValue,  // default value
                'entityModel' => $entity[0]->entityModel, // work model
                'required' => $required           // add rule "required field"
            ];

            $attr->save();
        }

        return $attr;
    }

    /**
     * @param EavAttribute $attribute
     * @return array|null|ActiveRecord
     */
    public function getEavAttributeValue(EavAttribute $attribute)
    {
        $attributeValue = $attribute->getEavAttributeValues()->where([
            'entityId' => $this->id
        ]);

        return $attributeValue->one();
    }

    /**
     * @param EavAttribute $attribute
     * @param $value
     * @return $this
     */
    public function setEavAttributeValue(EavAttribute $attribute, $value)
    {
        $attributeValue = $this->getEavAttributeValue($attribute);

        if (!$attributeValue instanceof EavAttributeValue) {

            $attributeValue = new EavAttributeValue();
            $attributeValue->entityId = $this->id;
            $attributeValue->attributeId = $attribute->id;
        }

        $attributeValue->value = $value;
        $attributeValue->save();

        return $this;
    }

    /**
     * @return $this
     */
    public function getEavAttributes()
    {
        return EavAttribute::find()
            ->joinWith('entity')
            ->where([
                'entityModel' => $this::className()
            ]);
    }

    /**
     * @return $this
     */
    public function getEavAttributesQuery()
    {
        return EavAttribute::find()
            ->joinWith('entity')
            ->where([
                'entityModel' => $this::className()
            ]);
    }


    /**
     * @return mixed
     */
    public function getEavAttributesByEntity()
    {
        return $this->getEavAttributesQuery()
            ->joinWith('eavAttributeValues val')
            ->andWhere(['val.entityId' => $this->id])
            ->all();
    }

    /**
     * @param $attributeName
     * @return array|null|ActiveRecord
     */
    public function getEavAttribute($attributeName)
    {
        return EavAttribute::find()
            ->joinWith('entity')
            ->where([
                'eav_entity.categoryId' => 1,
                'entityModel' => $this::className(),
                'name' => $attributeName
            ])->one();
    }

    /**
     * @param $attributes
     * @throws Exception
     */
    public function saveEavAttributes($attributes)
    {
        foreach ($attributes as $name => $value) {
            $attribute = $this->getEavAttribute($name);

            if (!$attribute instanceof EavAttribute) {
                $attribute = $this->createEavAttribute($name, $name);
            }

            $this->setEavAttributeValue($attribute, $value);
        }
    }

    /**
     * delete Eav Attributes 
     */
    public function deleteEavAttributes()
    {
        $eavAttributes = $this->getEavAttributesByEntity();
        foreach ($eavAttributes as $attribute) {
            /** @var EavAttributeValue $attribute */
            $attribute->delete();
        }
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function setAttribute($name, $value)
    {
        if ($this->hasAttribute($name)) {
            parent::setAttribute($name, $value);
        } else {
            $this->eavAttributes[$name] = $value;
        }
    }

    /**
     * @param array $values
     * @param bool $safeOnly
     */
    public function setAttributes($values, $safeOnly = true)
    {
        if (is_array($values)) {
            $attributes = array_flip($safeOnly ? $this->safeAttributes() : $this->attributes());
            foreach ($values as $name => $value) {
                if (isset($attributes[$name])) {
                    $this->$name = $value;
                } elseif ($safeOnly) {
                    $this->eavAttributes[$name] = $value;
                }
            }
        }
    }

    /**
     * @param string $name
     * @return array|mixed|null|string
     */
    public function __get($name)
    {
        if ($this->hasAttribute($name)) {
            return parent::__get($name);
        } elseif ($name == 'attributes') {
            return $this->getAttributes();
        }
        $attribute = $this->getEavAttribute($name);

        if ($attribute instanceof EavAttribute) {
            $value = $this->getEavAttributeValue($attribute);

            if ($value instanceof EavAttributeValue) {
                return $value->value;
            }

        }
        return null;
    }

    /**
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }
        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }
        foreach ($this->getEavAttributesByEntity() as $attr) {
            $value = $this->getEavAttributeValue($attr);
            $values[$attr->name] = $value->value;
        }
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * @return array
     */
    public function fields()
    {
        $names = parent::fields();

        foreach ($this->getEavAttributesByEntity() as $attr) {
            $names[] = $attr->name;
        }
        return $names;
    }

    /**
     * save eav attributes
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        $this->saveEavAttributes($this->eavAttributes);
        parent::afterSave($insert, $changedAttributes);

    }
}