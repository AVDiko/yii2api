<?php

namespace app\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "worker_history".
 *
 * @property integer $id
 * @property integer $worker_id
 * @property string $name
 * @property integer $active
 * @property string $history
 * @property string $date
 */
class WorkerHistory extends ActiveRecord
{

    public $from;
    public $to;
    public $unserialize;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker_history';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['worker_id', 'active'], 'integer'],
            [['history'], 'string'],
            [['date'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['from', 'to'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'worker_id' => 'Worker ID',
            'name' => 'Name',
            'active' => 'Active',
            'history' => 'History',
            'date' => 'Date',
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = $this::find();

        $dataProvider = new ActiveDataProvider(['query' => $query,
            'pagination' => false,
            'sort' => ['defaultOrder' => ['date' => SORT_DESC]]
        ]);

        $this->load($params);

        if ($this->from)
            $this->from = date('Y-m-d H:i:s', $this->from);

        if ($this->to)
            $this->to = date('Y-m-d H:i:s', $this->to);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            //return $dataProvider;
        }

        if (isset($this->active)) {
            $query->andFilterWhere(['active' => $this->active]);
        }

        if ($this->worker_id) {
            $query->andFilterWhere(['worker_id' => $this->worker_id]);
        }

        if ($this->name) {
            $query->andFilterWhere(['like', 'name', $this->name]);
        }

        if ($this->from) {
            $query->andFilterWhere(['>=', 'date', $this->from]);
        }

        if ($this->to) {
            $query->andFilterWhere(['<=', 'date', $this->to]);
        }
        
        return $dataProvider;
    }

    /**
     * @param null $names
     * @param array $except
     * @return array
     */
    public function getAttributes($names = null, $except = [])
    {
        $values = [];
        if ($names === null) {
            $names = $this->attributes();
        }

        foreach ($names as $name) {
            $values[$name] = $this->$name;
        }

        $attributes = $this->history;
        $attributes = unserialize($attributes);
        foreach ($attributes as $attribute => $value) {
            $values[$attribute] = $value;
        }
        
        foreach ($except as $name) {
            unset($values[$name]);
        }

        return $values;
    }

    /**
     * @param string $name
     * @return array|mixed|null
     */
    public function __get($name)
    {
        if ($this->hasAttribute($name)) {
            return parent::__get($name);
        } elseif ($name == 'attributes') {
            return $this->getAttributes();
        }

        if (isset($this->unserialize[$name])) {
            return $this->unserialize[$name];
        }

        return null;
    }

    /**
     * @return array
     */
    public function fields()
    {
        $names = parent::fields();

        $attributes = $this->history;
        $attributes = unserialize($attributes);
        foreach ($attributes as $attribute => $value) {
            $this->unserialize[$attribute] = $value;
            $names[] = $attribute;
        }
        unset($names['history']);

        return $names;
    }
}
