<?php
namespace app\models;

use yii\base\Behavior;
use yii\db\ActiveRecord;


class HistoryBehavior extends Behavior
{
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
            ActiveRecord::EVENT_AFTER_UPDATE => 'afterUpdate',
        ];

    }
    
    public $valueClass;

    public $unsetFields = ['id', 'name', 'active'];

    public function init()
    {
        assert(isset($this->valueClass));
    }

    /**
     * Create
     */
    public function afterInsert()
    {
        $this->saveHistory();
    }

    /**
     * Update & Delete
     */
    public function afterUpdate()
    {
        if ($this->owner->active == 1) {
            $this->saveHistory();
        } else {
            $this->saveHistory( 0);
        }
    }

    /**
     * @param int $active
     */
    protected function saveHistory($active = 1)
    {
        /** @var Worker $model */
        $model = $this->owner;
        $attributes = $model->attributes;
        
        foreach ($this->unsetFields as $unset) {
            unset($attributes[$unset]);
        }
        
        $history = new WorkerHistory();
        $history->worker_id = $model->id;
        $history->name = $model->name;
        $history->active = $active;
        $history->history = serialize($attributes);
        $history->date = date('Y-m-d H:i:s');
        $history->save();
    }
}