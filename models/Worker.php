<?php

namespace app\models;
use Exception;

/**
 * This is the model class for table "worker".
 *
 * @property integer $id
 * @property string $name
 * @property integer $active
 */
class Worker extends Eav
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'worker';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['active'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'active' => 'Active',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'history' => [
                'class' => HistoryBehavior::className(),
                'valueClass' => WorkerHistory::className(),
            ]
        ];
    }
    
    /**
     * @throws Exception
     */
    public function delete()
    {
        if ($this->active != 0) {
            $this->active = 0;
            $this->save();
            $this->deleteEavAttributes();
        } else {
            throw new Exception('this record already deleted');
        }
    }
}
