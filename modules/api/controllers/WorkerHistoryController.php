<?php
namespace app\modules\api\controllers;

use Yii;
use app\models\WorkerHistory;
use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class WorkerHistoryController extends ActiveController
{
    public $modelClass = 'app\models\WorkerHistory';

    public function behaviors()
    {
        return [
            ['class' => ContentNegotiator::className(),
                'only' => ['view', 'index'],
                'formats' => ['application/json' => Response::FORMAT_JSON,],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['view']);
        return $actions;
    }

    public function actionIndex()
    {
        $param["WorkerHistory"] = Yii::$app->request->queryParams;
        $searchModel = new WorkerHistory();
        $dataProvider = $searchModel->search($param);
        return $dataProvider;
    }
}