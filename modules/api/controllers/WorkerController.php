<?php
namespace app\modules\api\controllers;


use yii\rest\ActiveController;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class WorkerController extends ActiveController
{
    public $modelClass = 'app\models\Worker';

    public function behaviors()
    {
        return [
            ['class' => ContentNegotiator::className(),
                //'only' => ['view', 'create', 'update', 'delete'],
                'formats' => ['application/json' => Response::FORMAT_JSON,],
            ],
        ];
    }

    public function actions()
    {
        $actions = parent::actions();
        //unset($actions['view']);
        return $actions;
    }
}